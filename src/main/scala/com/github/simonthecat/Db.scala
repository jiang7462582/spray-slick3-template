package com.github.simonthecat

import com.github.simonthecat.domain.Users
import slick.driver.MySQLDriver.api._


object Db {

  implicit val db: Database = Database.forConfig("application.database.mysql")

  val usersTable = TableQuery[Users]

  def createSchema(): Unit = db.run(usersTable.schema.create)

}
